#!flask/bin/python
from flask import Flask, jsonify,request
from flask_cors import CORS
from flask import (

    make_response,

    abort,

)

from config import db

from models import (Emp)
app = Flask(__name__)
CORS(app)
emps = [
    {
        'id': 1,
        'name': u'Vinith',
        'position' : u'ceo',
        'email': u'vinith@gamil.com', 
	'dob': u'', 
        'salary': 18188
    },
    {
        'id': 1,
        'name': u'Mohan',
	'position' : u'employee',
        'email': u'Mohan@gamil.com', 
        'dob': u'', 
        'salary': 40000
    }
]

@app.route('/emp/employees', methods=['GET'])
def get_allemps():
    #empss = Emp.query.all()
    return jsonify({'emps': emps})


@app.route('/emp/employee/<int:emp_id>', methods=['GET'])
def get_emp(emp_id):
    emp = [emp for emp in emps if emp['id'] == emp_id]
    if len(emp) == 0:
        abort(404)
    return jsonify({'emp': emp[0]})


@app.route('/emp/employee', methods=['POST'])
def create_emp():
    if not request.json or not 'email' in request.json:
        abort(400)
    #empss = Emp.query.all()
    emp = {     
        'id': len(emps) + 1,
        'name': request.json['name'],
        'email': request.json.get('email', ""),
	'dob': request.json.get('dob', ""),
        'salary': request.json.get('salary', ""),
    }
    """ emp = Emp(request.json['name'],request.json.get('email', ""),request.json.get('salary', ""))
    db.session.add(emp)
    db.session.commit()"""
    emps.append(emp)
    return jsonify({'emp': emp}), 201

@app.route('/emp/employeeupdate', methods=['POST'])
def update_emp():
    emp = [emp for emp in emps if emp['id'] == request.json['id']]
    if len(emp) == 0:
        abort(404)
    if not request.json:
        abort(400)
   
    emp[0]['name'] = request.json.get('name', emp[0]['name'])
    emp[0]['email'] = request.json.get('email', emp[0]['email'])
    emp[0]['dob'] = request.json.get('dob', emp[0]['dob'])
    emp[0]['salary'] = request.json.get('salary', emp[0]['salary'])
    return jsonify({'emp': emp[0]}),201

@app.route('/emp/delemployee/<int:emp_id>', methods=['GET'])
def delete_emp(emp_id):
    emp = [emp for emp in emps if emp['id'] == emp_id]
    if len(emp) == 0:
        abort(404)
    emps.remove(emp[0])
    return jsonify({'result': True})


if __name__ == '__main__':
    app.run(debug=True)
