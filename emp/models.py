from datetime import datetime
from config import db


class Emp(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(250), nullable=False)
    salary = db.Column(db.Integer, nullable=False)
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'),
        nullable=False)
    team = db.relationship('Team',
        backref=db.backref('emp', lazy=True))

    def __repr__(self):
        return '<Emp %r>' % self.full_name


class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    teamname = db.Column(db.String(80), nullable=False)
    	
    def __repr__(self):
        return '<Team %r>' % self.teamname

"""class EmpSchema(ma.SQLAlchemyAutoSchema):


    class Meta:

        model = Emp

        include_fk = True  """
