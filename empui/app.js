var app = angular.module('plunker', []);

app.controller('MainCtrl', function($scope, $http, $log) {
  vm = this;
  $scope.baseurl = 'http://127.0.0.1:5000/emp/'
  $scope.emp={};
  $scope.emps = [];
    $scope.emp['position'] = 'employee';
  $scope.name = 'Template User'; // anything called $scope.something here will be accessed as {{something}} in the HTML template

   $scope.schedules = {
        data: [{
            id: 'id1',
            Name: 'name1'
        }, {
            id: 'id2',
            Name: 'name2'
        }]
    };

    $scope.submitDet = function(){
        if($scope.emp.id == undefined){
            $http.post($scope.baseurl+'employee', JSON.stringify($scope.emp)).then(function (response) {

                if (response.data)
                
                $scope.msg = "Post Data Submitted Successfully!";
                    alert($scope.msg);
                    $scope.getEmp(); 
                
                }, function (response) {
                
                $scope.msg = "Service not Exists";
                
                $scope.statusval = response.status;
                
                $scope.statustext = response.statusText;
                
                $scope.headers = response.headers();
                
                });
    

        }else{
            
            $http.post($scope.baseurl+'employeeupdate', JSON.stringify($scope.emp)).then(function (response) {

                if (response.data)
                
                $scope.msg = "Data Updated Successfully!";
                alert($scope.msg);
                $scope.getEmp(); 
                
                }, function (response) {
                
                $scope.msg = "Service not Exists";
                
                $scope.statusval = response.status;
                
                $scope.statustext = response.statusText;
                
                $scope.headers = response.headers();
                
                });
    
        }
        


    };
    $scope.empEdit = function(id){

        $http.get($scope.baseurl+'employee/'+id)
        .then(function(response) {
            
          $scope.emp = response.data.emp;
        });



    }

    $scope.empDelete = function(id){

        $http.get($scope.baseurl+'delemployee/'+id)
        .then(function(response) {
            if(response.data.result){
                alert("Employee deleted successfully");
                $scope.getEmp(); 
            }
          
          
        });



    }



    $scope.getEmp = function(){

        $http.get($scope.baseurl+'employees')
  .then(function(response) {
      
    $scope.emps = response.data.emps;
  });


    }
  
});
app.directive('input', inputReadOnly);

function inputReadOnly() {
    var directive = {
        link: link,
        scope: false,
        transclude: true,
        restrict: 'E'
    };
    return directive;

    function link(scope, element, attrs) {
        if (!scope.vm) return;
        element.attr('readOnly', scope.vm.access === 'View');
    }
}

