# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains Employee management portal using Python flask framework and Angularjs. 

### How do I get set up? ###

1. Install Python 2.7 or Python 3.0

2. Install flask using "pip install flask"

3. Install flask-SQLAlchemy ORM using "pip install flask-SQLAlchemy"

4. Install CORS to handle CORS orgin issue from client

5. Once install all libraries run the app by running command "python empapp.py" in shell and then open the index.html from empui folder. Angularjs plugins are already added to that page. Configure the base url in app.js  based on flask server running localhost url and port. 




